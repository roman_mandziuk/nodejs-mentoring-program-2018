import fs from 'fs';
import path from 'path';
import EventEmitter from 'events';
import csv from 'csvtojson';
import csvSyncParse from 'csv-parse/lib/sync';

export default class Importer extends EventEmitter {
  constructor (Watcher) {
    super();

    Watcher.on('changed', (files, path) => this._convertCSVToJSON(files, path));
  }

  _convertCSVToJSON (files, dirPath) {
    const csvFilesPath = files
      .filter(file => file.indexOf('.csv') !== -1)
      .map(file => path.join(dirPath, file));

    Promise.all(csvFilesPath.map(async csvFilePath => {
      const importedJson = await this.import(csvFilePath);

      this.emit('json-from-csv-imported', importedJson);
    }));
  }

  async import (path) {
    try {
      const jsonFromCSV = await csv().fromFile(path);
      return jsonFromCSV;
    } catch (error) {
      throw error;
    }
  }

  importSync (path) {
    const csvFileContent = fs.readFileSync(path, 'utf8');

    return csvSyncParse(csvFileContent);
  }
}
