import EventEmitter from 'events';
import { promisify } from 'util';
import fs from 'fs';
import path from 'path';

const readDirAsync = promisify(fs.readdir);
const statAsync = promisify(fs.stat);

export default class DirWatcher extends EventEmitter {
  constructor (dirPath, delay) {
    super();

    this._dirPath = dirPath;
    this._delay = delay;
  }

  watch() {
    let previuosFilesStats = [];

    setInterval(() => {
      _watch(this._dirPath);
    }, this._delay);

    const _watch = async dirPath => {
      try {
        const filesNames = await readDirAsync(dirPath);
  
        const currentFileStats = await Promise.all(filesNames.map(async fileName => {
          const fileStats = await statAsync(path.join(dirPath, fileName));
          const lastModifiedTime = fileStats.mtimeMs;
  
          return {
            fileName: fileName,
            lastModifiedTime
          };
        }));
  
        const changedFiles = currentFileStats
          .filter(currentFile => {
            const previousFile = previuosFilesStats.find(previousFile => {
              return previousFile.fileName === currentFile.fileName;
            });
  
            return previousFile ?
              previousFile.lastModifiedTime !== currentFile.lastModifiedTime : true;
          }).map(file => file.fileName);

        if (changedFiles.length) {
          this.emit('changed', changedFiles, dirPath);
        }
  
        previuosFilesStats = currentFileStats;
      } catch (error) {
        throw error;
      }
    };
  }
}
