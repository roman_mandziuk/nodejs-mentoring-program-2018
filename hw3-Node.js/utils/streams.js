import yargs from 'yargs';

import argvOptions from './command-line-options';
import supportedFunctions from './command-line-functions';
import CommandActionHandler from './command-action-handler';

const CmdLineActionHandler = new CommandActionHandler();
const processArgv = process.argv.slice(2);
const { argv } = yargs.options(argvOptions);
const { action } = argv;

if (!processArgv.length) {
  yargs.showHelp();
}

if (supportedFunctions.hasOwnProperty(action)) {
  CmdLineActionHandler[action](argv);
} else {
  yargs.showHelp();
}
