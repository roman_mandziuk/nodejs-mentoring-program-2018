import fs from 'fs';
import readline from 'readline';
import csv from 'csvtojson';
import path from 'path';
import { promisify } from 'util';

import OptionsNames from './command-line-options-names';
import MESSAGES from './comman-line-messages';

const readDirAsync = promisify(fs.readdir);

export default class CommandActionHandler {
  reverse (argv) {
    this._stdinToStdout(MESSAGES.ENTER_TEXT, this._capitalizeString);
  }
  
  transform (argv) {
    this._stdinToStdout(MESSAGES.ENTER_TEXT, this._capitalizeString);
  }
  
  outputFile (argv) {
    if (this._isOptionMissed(argv, OptionsNames.FILE)) {
      console.log(`${MESSAGES.MISSED_OPTION} --${OptionsNames.FILE}`);
      
      return;
    }

    let { file:filePath } = argv;
    filePath = this._transfromPathToAbsolute(filePath);

    const readStream = fs.createReadStream(filePath);
  
    readStream.pipe(process.stdout);
  }

  convertFromFile (argv) {
    if (this._isOptionMissed(argv, OptionsNames.FILE)) {
      console.log(`${MESSAGES.MISSED_OPTION} --${OptionsNames.FILE}`);

      return;
    }

    let { file:filePath } = argv;
    filePath = this._transfromPathToAbsolute(filePath);

    if (!this._isCSVFile(filePath)) {
      console.log(MESSAGES.ONLY_CSV_FILE);

      return;
    }

    const readStream = fs.createReadStream(filePath);

    readStream.pipe(csv()).pipe(process.stdout);

    readStream
      .on('error', (error) => {
        if (error.code === 'ENOENT') {        
          console.log(MESSAGES.WRONG_FILE_PATH);
        }

        throw new Error(error);
      });
  }

  convertToFile (argv) {
    if (this._isOptionMissed(argv, OptionsNames.FILE)) {
      console.log(`${MESSAGES.MISSED_OPTION} --${OptionsNames.FILE}`);
      return;
    }

    let { file:filePath } = argv;
    filePath = this._transfromPathToAbsolute(filePath);

    if (!this._isCSVFile(filePath)) {
      console.log(MESSAGES.ONLY_CSV_FILE);

      return;
    }

    const { dir:jsonFileDir, name:jsonFileName } = path.parse(filePath);
    const jsonFilePath = path.join(jsonFileDir, `${jsonFileName}.json`);

    const readStream = fs.createReadStream(filePath);
    const writeStream = fs.createWriteStream(jsonFilePath);

    readStream.pipe(csv()).pipe(writeStream);

    readStream
      .on('error', (error) => {
        writeStream.destroy();

        if (error.code === 'ENOENT') {        
          console.log(MESSAGES.WRONG_FILE_PATH);
        }

        throw new Error(error);
      });

    console.log(MESSAGES.WRITE_TO_FILE_SUCCESS);
  }

  async cssBundler (argv) {
    if (this._isOptionMissed(argv, OptionsNames.PATH)) {
      console.log(`${MESSAGES.MISSED_OPTION} --${OptionsNames.PATH}`);
      return;
    }

    let { path:dirPath } = argv;

    const CSSFiles = await this._getCSSFilesPathFromDirectory(dirPath)
      .catch(error => {
        if (error.code === 'ENOENT') {        
          console.log(MESSAGES.WRONG_PATH);
        }

        throw new Error(error);
      });

    if (!CSSFiles.length) {
      console.log(MESSAGES.NO_CSS_FILES_IN_DIRECTORY);

      return;
    }

    const filesContent = await this._readFiles(CSSFiles);
    const writeStream = fs.createWriteStream(path.join(dirPath, 'bundle.css'));
    
    filesContent.forEach(fileContent => {
      writeStream.write(fileContent + '\n', 'utf-8');
    });
    
    writeStream
      .on('error', (error) => { throw new Error(error); });
      
    console.log(MESSAGES.WRITE_TO_FILE_SUCCESS);
  }

  _stdinToStdout (question, transformer) {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    
    rl.question(question, (answer) => {
      const data = transformer(answer);
      rl.write(data);
  
      rl.close();
    });
  }

  async _readFiles (filesDir) {
    return await Promise.all(filesDir
      .map(fileDir =>
        new Promise((resolve, reject) => {
          const readStream = fs.createReadStream(fileDir, 'utf8');
          let data = '';
      
          readStream
            .on('data', chunk => { data += chunk; })
            .on('end', () => { resolve(data); })
            .on('error', error => reject(error));
        })
      ));
  }

  _isOptionMissed (argv, ...options) {
    return options.some(option => !argv.hasOwnProperty(option));
  }

  _reverseString (string) {
    return string && string.toString().split('').reverse().join('');
  }
  
  _capitalizeString (string) {
    return string && string.toString().toUpperCase();
  }

  _transfromPathToAbsolute (filePath) {
    return path.isAbsolute(filePath) ? filePath : path.resolve(filePath);
  }

  _isCSVFile (filePath) {
    return path.extname(filePath) === '.csv';
  }

  _isCSSFile (filePath) {
    return path.extname(filePath) === '.css';
  }

  async _getCSSFilesPathFromDirectory (directory) {
    directory = this._transfromPathToAbsolute(directory);
    const filesNames = await readDirAsync(directory)
      .catch(error => {
        if (error.code === 'ENOENT') {        
          console.log(MESSAGES.WRONG_PATH);
        }

        throw new Error(error);
      });

    return filesNames
      .filter(fileName => this._isCSSFile(fileName))
      .map(fileName => path.join(directory, fileName));
  }
}
