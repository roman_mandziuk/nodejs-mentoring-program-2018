export default {
  help: {
    key: 'help',
    description: 'Show help',
    alias: 'h',
    type: 'string'
  },
  action: {
    key: 'action',
    description: 'Action to do',
    alias: 'a',
    type: 'string',
    requiresArg: true
  },
  file: {
    key: 'file',
    description: 'File name',
    alias: 'f',
    type: 'string',
    requiresArg: true
  },
  path: {
    key: 'path',
    description: 'Directory path',
    alias: 'p',
    type: 'string',
    requiresArg: true
  }
};
