export default {
  ENTER_TEXT: 'Please enter text: ',
  MISSED_OPTION: 'You missed an option for this command',
  NO_CSS_FILES_IN_DIRECTORY: 'No CSS files present in this directory',
  ONLY_CSV_FILE: 'Please choose CSV file',
  WRONG_FILE_PATH: 'No file in this directory',
  WRONG_PATH: 'Path does\'nt exist',
  WRITE_TO_FILE_SUCCESS: 'File has been written'
};
