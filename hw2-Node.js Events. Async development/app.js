import * as config from './config/config.json';
import { User, Product } from './models/index.js';

import { DirWatcher, Importer } from './modules/index.js';

console.log(config.name);

let MyUser = new User();
let MyProduct = new Product();

const myDirWatcher = new DirWatcher('./data/', 5000);
myDirWatcher.watch();

const myImporter = new Importer(myDirWatcher);
myImporter.on('json-from-csv-imported', (fileContent => {
  console.log(fileContent, 'imported on dir changed!');
}));

console.log(myImporter.importSync('./data/C2ImportGroupsSample.csv'), 'static sync file imported!');

myImporter.import('./data/C2ImportFamRelSample.csv')
  .then(fileContent => console.log(fileContent, 'static async file imported!'))
  .catch(error => console.error(error));
